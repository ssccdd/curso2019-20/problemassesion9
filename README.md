[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 9

Problemas propuestos para la Sesión 9 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

En esta sesión de prácticas se proponen ejercicio para trabajar con el paquete [java.util.concurrent](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html). En los ejercicios nos centramos en la utilización de variables atómicas definidas en [java.util.concurrent.atomic](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/package-frame.html). Seguimos utilizando la interface [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html "interface in java.util.concurrent") para la ejecución de las tareas. Los ejercicios son diferentes para cada grupo:

- Grupos 1, 3 y 4
- Grupo 2

## Grupos 1, 3 y 4

El ejercicio consiste en realizar las modificaciones necesarias a la solución del problema propuesto en la [Sesión 5](https://gitlab.com/ssccdd/curso2019-20/problemassesion5#grupo-1). Las modificaciones a realizar son las siguientes:

1. La tarea `Fabricante` debe actualizar el inventario para el `TipoComponente` que simula fabricar. El inventario es una estructura compartida del tipo [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html "class in java.util.concurrent.atomic") donde el índice representa al `TipoComponente`. Para ello hay que utilizar el método apropiado que implemente la operación **Compare and Set** para asegurar el valor correcto para el inventario. 
	- Hay que añadir una nueva variable de instancia para poder trabajar con el inventario compartido. 
2. La tarea `Proveedor` debe modificarse para:
	- Incluir un identificador para diferenciar diferentes tareas de este tipo
	- Modificar el pedido para que sea una estructura segura que implemente la interface [`BlockingQueue`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/BlockingQueue.html "interface in java.util.concurrent"). A esta estructura se añadirán los ordenadores que cada `Proveedor` complete.
	- Añadirle una lista de trabajo que representa los ordenadores que está montando el `Proveedor` durante su ejecución.
	- Una estructura compartida de tipo `AtomicIntegerArray` que permitirá llevar la cuenta de ordenadores completados y ordenadores incompletos.
		- Cada vez que un `Proveedor` cree un nuevo ordenador deberá actualizar la cuenta de ordenadores incompletos.
		- Cada vez que un `Proveedor` complete un ordenador deberá actualizar la cuenta de ordenadores completos y disminuir la de ordenadores incompletos.
3. La tarea `CrearComponente` para que cree correctamente la nueva tarea `Fabricante`.
4. El `Hilo Principal` debe modificarse para:
	- Inicializar las nuevas variables compartidas entre las tareas.
	- Se crean un número de tareas `Proveedor` que viene definido por la constante `PROVEEDORES`.
	- Antes de finalizar deberá mostrar, además del pedido de ordenadores completados, el inventario de piezas que se han fabricado y el número de ordenadores que se han completado y los que están incompletos por las tareas `Proveedor`.

Hay que utilizar los elementos definidos en la interface `Constantes`.

## Grupo 2

## Enunciado de la práctica

### Introducción

En una granja de rendering (render farm) se generan escenas que luego se renderizan. Todo el proceso se realiza concurrentemente. Hay generadores de escenas que recopilan todo lo necesario para renderizar una escena (modelos 3D, texturas, materiales, etc.) y lo ponen a disposición de los renderizadores que construyen las imágenes con la especificación de la escena, mediante una lista de escenas común. (En esta sesión las tareas son simuladas con sleep).

### Novedades de esta sesión

Las escenas tienen una **prioridad** que puede ser `ALTA`, `MEDIA` o `BAJA`. Las escenas con mas alta prioridad se deben renderizar primero. Sólo si no hay escenas de prioridad `ALTA` se renderizan escenes de prioridad `MEDIA` y sólo si no hay escenas de prioridad `ALTA` o `MEDIA` se renderizan escenas de prioridad `BAJA`. Para ello, la lista de escenas pendientes se gestionará mediante una `PriorityBlockingQueue`.

Tanto la prioridad de una escena, como otros parámetros necesarios para la ejecución del programa, como duración de las escenas, etc. se generan aleatoriamente. Para ello se usará la clase `ThreadLocalRandom` que permite generar números aleatorios de forma concurrente y muy eficiente.

La bandera booleana que se usa para avisar a los renderizadores de escenas de que todos los generadores han terminado debe implementarse mediante la variable atómica `AtomicBoolean`.

### Normas e instrucciones

- Descargar el archivo comprimido `Sesion9.zip` que contiene el proyecto NetBean de la sesión donde se encuentran algunas clases y elementos programados que se deben utilizar obligatoriamente. 

- Renombrar el proyecto NetBean, no olvidéis marcar la casilla para cambiar el directorio también, con el siguiente formato: `[ApellidoNombre]Sesion09`. Donde `[ApellidoNombre]` debe sustituirse por los de cada estudiante.

- A la finalización del ejercicio se deberá entregar un archivo comprimido `.zip` que contenga el proyecto NetBeans con la solución con el siguiente nombre: `[ApellidoNombre]Sesion09.zip`.

### Condiciones generales

Hay una estructura de datos de tipo `PriorityBlockingQueue` compartida en la que se almacenan las escenas.

Las escenas se generan por los generadores de escenas. Cada generador de escenas debe generar un número aleatorio de escenas. Los generadores de escenas terminan su ejecución cuando generan todas las escenas asignadas. El proceso de generar una escena consume un tiempo aleatorio que se simula mediante `sleep`. Una vez terminado ese tiempo la escena está lista y puede añadirse a la estructura de datos compartida.

Los renderizadores de escenas acceden a la estructura de datos compartida, extraen la primera escena disponible y la renderizan. Como la estructura que almacena las escenas está ordenada por prioridad, la primera va a ser siempre una de mayor prioridad que todas las demás. El proceso de renderizado consume un tiempo que se guarda en la propia escena junto con su prioridad. Los renderizadores de escenas terminan su ejecución cuando todos los generadores de escenas han terminado y no quedan escena en la estrucura de datos compartida.

### Elementos a programar

- `Escena`

	- Representa a una escena a renderizar en la granja de rendering.

	- Contiene un identificador de escena `ID`, un tiempo de renderización `renderTime` y una prioridad `priority`. Tanto el tiempo de renderización como su prioridad deben generarse de forma aleatoria.

- `Finalizador`

	- Se encarga de avisar a los objetos `RenderizadorEscenas` de que todos los objetos `GeneradorEscenas` han terminado su ejecución. Para ello se debe utilizar una bandera booleana implementada mediante `AtomicBoolean`.

- `GeneradorEscena`

	- Tiene un identificador `ID`.

	- Tiene acceso a la estructura de datos compartida donde se almacenan las escenas.

	- Genera un número de escenas aleatorio entre `MIN_ESCENAS_X_GENERADOR` y `MIN_ESCENAS_X_GENERADOR + VARIACION_ESCENAS_X_GENERADOR` y termina su ejecución. Para cada escena que se genera:

		- Espera un tiempo aleatorio entre `MIN_TIEMPO_GENERACION` y `MIN_TIEMPO_GENERACION` + `VARIACION_GENERACION`. Este tiempo simula lo que se tardaría en recopilar todos los elementos de la escena en una aplicación real.

		- Construye un identificador para la escena formado por el `ID` del generador seguido de un número secuencial.

		- Construye una prioridad con valor `ALTA`, `MEDIA` o `BAJA`, que se construye de forma aleatoria.

		- Construye un tiempo de generación aleatorio entre `MIN_DURACION_ESCENA` y `MIN_DURACION_ESCENA + VARIACION_DURACION`.

		- Con esa información, se construye un objeto de clase `Escena` y se añade a la estructura de datos compartida que almacena las escenas pendientes.

	- Cuando se completa la tarea se devuelve una lista con las escenas que se han generado.

- `RenderizadorEscena`

	- Tiene un identificador `ID`.

	- Tiene acceso a la estructura de datos compartida donde se almacenan las escenas.

	- Renderiza escenas en un ciclo que termina cuando le avisan de que los generadores han terminado ya y la estructura de datos compartida donde se almacenan las escenas está vacía.

		- En cada paso se extrae la primera escena de la estructura de escenas pendientes.

		- Se espera el tiempo de renderización que se guarda en la propia escena, en su atributo: `renderTime`.
	
	- Cuando se completa la tarea se devuelve una lista con las escenas que se han renderizado.

- `Hilo Principal`

	- Crea la estructura compartida, ordenada por prioridad, para almacenar las escenas pendientes.

	- Crea e inicia la ejecución de `NUM_GENERADORES` tareas `GeneradorEscenas`.

	- Crea e inicia la ejecución de `NUM_RENDERIZADORES` tareas `RenderizadorEscenas`.

	- Espera a que todas las tareas `GeneradorEscenas` hayan terminado y entonces notifica a las tareas `RenderizadorEscenas` que deben continuar pero sólo hasta que la estructura con las escenas pendientes se quede vacía.

	- Espera hasta que todas las tareas estén completas.

	- Muestra para cada tarea `GeneradorEscenas` y cada tarea `RenderizadorEscenas` las escenas que se han generado / renderizado.

	- Muestra las escenas que hayan quedado sin renderizar en la estructura compartida de escenas pendientes (debería estar vacía completamente).


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzMTczNTU3NTAsMTYyNzI2ODI1Ml19
-->