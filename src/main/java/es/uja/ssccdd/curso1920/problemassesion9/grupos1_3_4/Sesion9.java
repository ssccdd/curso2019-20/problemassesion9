/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4;

import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.COMPONENTES;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.CREAR_COMPONENTE;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.ESPERA_FINALIZACION;
import es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.Estado;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.FABRICANTES;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.PROVEEDORES;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.TipoComponente;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Sesion9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        CompletionService<Componente> fabricacion;
        CountDownLatch esperaFinalizacion;
        AtomicIntegerArray inventario;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        BlockingQueue<Ordenador> pedido;
        AtomicIntegerArray completados;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(FABRICANTES);
        fabricacion = new ExecutorCompletionService(ejecucion);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        inventario = new AtomicIntegerArray(COMPONENTES.length);
        listaTareas = new ArrayList();
        pedido = new LinkedBlockingQueue();
        completados = new AtomicIntegerArray(Estado.values().length);
        
        // Se añade la tarea de fabricación de componentes que se repetirá cíclicamente
        CrearComponente crear = new CrearComponente(fabricacion,inventario,listaTareas);
        tarea = ejecucionSistema.scheduleAtFixedRate(crear, INICIO, CREAR_COMPONENTE, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se añaden las tareas de los proveedores para preparar el pedido de ordenadores
        for(int i = 0; i < PROVEEDORES; i++) {
            Proveedor proveedor = new Proveedor(i,fabricacion,pedido,completados);
            tarea = ejecucionSistema.submit(proveedor);
            listaTareas.add(tarea); // Se añade para la cancelación
        }
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) el inventario de componentes es \n______________");
        for(TipoComponente componente : COMPONENTES)
            System.out.println("Total " + componente + " : " + inventario.get(componente.ordinal()));
        System.out.println("______________");
        
        System.out.println("HILO(Principal) el pedido obtenido es \n______________");
        for( Ordenador ordenador : pedido )
            System.out.println(ordenador.toString());
        System.out.println("______________");
        
        System.out.println("HILO(Principal) el inventario de ordenadores es \n______________");
        for(Estado estado : Estado.values())
            System.out.println("Total " + estado + " : " + completados.get(estado.ordinal()) + " ordenadores");
        System.out.println("______________");
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }  
}
