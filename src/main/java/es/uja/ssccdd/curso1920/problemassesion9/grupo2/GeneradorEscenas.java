/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.MIN_DURACION_ESCENA;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.MIN_ESCENAS_X_GENERADOR;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.MIN_TIEMPO_GENERACION;
import es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.Prioridad;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.VARIACION_DURACION;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.VARIACION_ESCENAS_X_GENERADOR;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.VARIACION_GENERACION;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que simula la generacion de escenas
 * @author fconde
 */
public class GeneradorEscenas implements Callable<List<Escena>> {

    private final String ID;
    private final PriorityBlockingQueue queue;
    private final CyclicBarrier barrier;

    public GeneradorEscenas(String ID, PriorityBlockingQueue queue, CyclicBarrier barrier) {
        this.ID = ID;
        this.queue = queue;
        this.barrier = barrier;
    }

    @Override
    public List<Escena> call() {
        System.out.println("Ejecutando "+ID);
        ArrayList<Escena> resultado = new ArrayList<Escena>();
        int numEscenas = MIN_ESCENAS_X_GENERADOR + ThreadLocalRandom.current().nextInt(VARIACION_ESCENAS_X_GENERADOR);
        for (int i=0; i<numEscenas; i++) {
            int tiempo = MIN_TIEMPO_GENERACION+ThreadLocalRandom.current().nextInt(VARIACION_GENERACION);
            try {
                TimeUnit.SECONDS.sleep(tiempo);
            } catch (InterruptedException ex) {
                Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
            }
            int tiempoRenderizacion = MIN_DURACION_ESCENA + ThreadLocalRandom.current().nextInt(VARIACION_DURACION);
            Escena escena = new Escena(ID+"-"+i, tiempoRenderizacion, Prioridad.getPrioridad());
            queue.add(escena);
            resultado.add(escena);
        }
        System.out.println("Terminando ejecución "+ID);
        try {
            barrier.await();
        } catch (InterruptedException ex) {
            Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BrokenBarrierException ex) {
            Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }
}
