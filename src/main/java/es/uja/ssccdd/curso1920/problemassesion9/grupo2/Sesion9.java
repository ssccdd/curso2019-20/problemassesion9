/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.NUM_GENERADORES;
import static es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.NUM_RENDERIZADORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase principal.
 * @author fconde
 */
public class Sesion9 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecucion\n");

        PriorityBlockingQueue queue = new PriorityBlockingQueue();
        Finalizador finalizador = new Finalizador();
        CyclicBarrier barrier = new CyclicBarrier(NUM_GENERADORES, finalizador);

        ArrayList<Callable<List<Escena>>> tareas = new ArrayList<Callable<List<Escena>>>();

        for (int i=0; i<NUM_GENERADORES; i++) {
            GeneradorEscenas generador = new GeneradorEscenas("GEN-"+i, queue, barrier);
            tareas.add(generador);
        }
        for (int i=0; i<NUM_RENDERIZADORES; i++) {
            RenderizadorEscenas renderizador = new RenderizadorEscenas("REND-"+i, queue);
            tareas.add(renderizador);
        }

        ExecutorService executor = Executors.newFixedThreadPool(NUM_GENERADORES+NUM_RENDERIZADORES);

        List<Future<List<Escena>>> resultados = null;
        try {
            resultados = executor.invokeAll(tareas);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion9.class.getName()).log(Level.SEVERE, null, ex);
        }

        // - Hacer que el marco de ejecucion no pueda recibir nuevas tareas
        executor.shutdown();

        System.out.println("\n** Hilo(PRINCIPAL): Todos los hilos han terminado\n");

        System.out.println("** Hilo(PRINCIPAL): MOSTRANDO GENERADORES");
        for (int i=0; i<NUM_GENERADORES; i++) {
            Future<List<Escena>> resultado = resultados.get(i);
            List<Escena> res;
            try {
                res = resultado.get();
                System.out.println(res);
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion9.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(Sesion9.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("\n** Hilo(PRINCIPAL): MOSTRANDO RENDERIZADORES");
        for (int i=NUM_GENERADORES; i<NUM_GENERADORES+NUM_RENDERIZADORES; i++) {
            Future<List<Escena>> resultado = resultados.get(i);
            List<Escena> res;
            try {
                res = resultado.get();
                System.out.println(res);
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion9.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(Sesion9.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println("\n** Hilo(PRINCIPAL): MOSTRANDO COLA DE ESCENAS CON PRIORIDAD");
        System.out.println(queue);
        
        System.out.println("\n** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
    }    
}
