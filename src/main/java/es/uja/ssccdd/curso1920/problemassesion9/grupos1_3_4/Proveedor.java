/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4;

import es.uja.ssccdd.curso1920.problemassesion9.grupos1_3_4.Constantes.Estado;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final int iD;
    private final CompletionService<Componente> fabricacion;
    private final BlockingQueue<Ordenador> pedido;
    private final AtomicIntegerArray inventario;
    private final List<Ordenador> trabajo;
    
    // Variable de clase
    private static int numSerie = 0;

    public Proveedor(int iD, CompletionService<Componente> fabricacion, BlockingQueue<Ordenador> pedido, 
                                                                        AtomicIntegerArray inventario) {
        this.iD = iD;
        this.fabricacion = fabricacion;
        this.pedido = pedido;
        this.inventario = inventario;
        this.trabajo = new ArrayList();
    }

    

    @Override
    public void run() {
        System.out.println("TAREA-Proveedor-" + iD + " empieza la preparación del pedido de ordenadores");
        
        try {
            
            while( true )
                prepararPedido();
            
        } catch ( InterruptedException ex ) {
            System.out.println("TAREA-Proveedor-" + iD + " finaliza el pedido de ordenadores");
        } catch (ExecutionException ex) {
            System.out.println("TAREA-Proveedor-" + iD + " ERROR en la fabricación");
        }
    }
    
    private void prepararPedido() throws InterruptedException, ExecutionException {
        Future<Componente> fabricado;
        Componente componente;
        Ordenador ordenador;
        Iterator it = trabajo.iterator();
        boolean asignado = false;
        
        // Espera hasta tener un componente
        fabricado = fabricacion.take();
        componente = fabricado.get();
        
        // Asigna componente a un ordenador si es posible
        while ( it.hasNext() && !asignado ) {
            ordenador = (Ordenador) it.next();
            asignado = ordenador.addComponente(componente);
            
            // Si el ordenador se ha completado lo añadimos al pedido y
            // actualizamos el inventario del proveedor
            if( asignado && ordenador.ordenadorCompleto() ) {
                pedido.put(ordenador);
                inventario.getAndIncrement(Estado.COMPLETADO.ordinal());
                inventario.getAndDecrement(Estado.NO_COMPLETADO.ordinal());
            }
        }
        
        // Si no se ha podido asignar el componente se crea un nuevo ordenador
        // al pedido y se le asigna el componente
        if( !asignado ) {
            ordenador = new Ordenador();
            ordenador.setiD("Proveedor("+getNumSerie()+")");
            ordenador.addComponente(componente);
            trabajo.add(ordenador);
            inventario.getAndIncrement(Estado.NO_COMPLETADO.ordinal());
        }
    }

    public int getiD() {
        return iD;
    }
    
    /**
     * Identificador único para crear el ordenador
     * @return el número de idOrdenador
     */
    private static synchronized int getNumSerie() {
        return ++numSerie;
    }
}
