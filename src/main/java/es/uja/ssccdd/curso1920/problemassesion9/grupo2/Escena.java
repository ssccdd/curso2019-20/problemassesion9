/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupo2;

import es.uja.ssccdd.curso1920.problemassesion9.grupo2.Constantes.Prioridad;


/**
 * Escena. Hay que implementar el metodo compareTo.
 * @author fconde
 */
public class Escena implements Comparable {
    
    private final String ID;
    private final int renderTime;
    private final Prioridad priority;

    public Escena(String ID, int renderTime, Prioridad priority) {
        this.ID = ID;
        this.renderTime = renderTime;
        this.priority = priority;
    }

    public String getID() {
        return ID;
    }
    
    public Prioridad getPriority() {
        return priority;
    }
    
    public int getRenderTime() {
        return renderTime;
    }

    @Override
    public String toString() {
        return "Escena(" + ID + ", render time: " + renderTime + ", prioridad: " + priority + ")";
    }

    @Override
    public int compareTo(Object o) {
        if (this.priority.ordinal() > ((Escena)o).getPriority().ordinal()) {
            return -1;
        } else if (this.priority.ordinal() < ((Escena)o).getPriority().ordinal()) {
            return 1;
        } else {
            return 0;
        }
    }
}
