/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion9.grupo2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que simula la renderizacion de escenas.
 * @author fconde
 */
public class RenderizadorEscenas implements Callable<List<Escena>> {

    private static boolean terminar = false;
    
    private final String ID;
    private final PriorityBlockingQueue queue;

    public RenderizadorEscenas(String ID, PriorityBlockingQueue queue) {
        this.ID = ID;
        this.queue = queue;
    }

    public static void setTerminar(boolean terminar) {
        RenderizadorEscenas.terminar = terminar;
    }

    @Override
    public List<Escena> call() {
        System.out.println("Ejecutando "+ID);
        ArrayList<Escena> resultado = new ArrayList<Escena>();
        while(true) {
            if ((queue.size() == 0) && (RenderizadorEscenas.terminar)) {
                System.out.println("Terminando ejecución " + ID);
                return resultado;
            } else {
                try {
                    Escena escena = (Escena) queue.take();
                    resultado.add(escena);
                    TimeUnit.SECONDS.sleep(escena.getRenderTime());
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
